# Docker image to provide a GCC cross-compiler for the MLF2 software
FROM i386/debian:11-slim
LABEL org.opencontainers.image.authors="mikek@apl.uw.edu"

RUN dpkg --add-architecture i386 && apt-get update && \
    apt-get -y --no-install-recommends install \
        curl \
        ca-certificates \
        git \
        scons \
        cmake \
        make \
        zip \
        unzip \
        && \
    apt-get clean

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN curl -sS -L "https://bitbucket.org/uwaplmlf2/tt8-sys/downloads/tt8gcc_1.1-gcc295_i386.tar.gz" |\
    tar -x -z -f -
COPY ./apps/* /usr/bin/

# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LANGUAGE=C.UTF-8

ENV PATH=/usr/local/bin:$PATH
