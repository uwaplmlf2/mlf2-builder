# GCC Cross Compiler for MLF2

This repository is used to build a Docker container with the GCC cross compiler and C libraries for building MLF2 TT8 software.

## Installation

Download the latest image from Docker Hub.

``` shell
docker pull mfkenney/mlf2builder:latest
```

## Build from source

Clone this repository and run the build script; `./build.sh`

## Running

``` shell
docker run -it --rm -v $(pwd):/build --workdir /build mfkenney/mlf2builder:latest /bin/bash
```

## Tools

The scripts from the `./apps` directory are available under `/usr/bin`.

  - **bbupload.sh** - upload a release package to Bitbucket
  - **merge-ballast.sh** - integrate the latest revision of the ballasting code
  - **merge-ballast-cm.sh** - version of merge-ballast.sh that uses the new CMake-based build system
  - **mlf2release.sh** - build a new release of the mission software and publish to Bitbucket
  - **mlf2release-pl.sh** - version of mlf2release.sh that can be run in a Bitbucket Pipeline
  - **librelease.sh** - build a new release of an MLF2 library and publish to Bitbucket.
  - **getlibs.sh** - downloads the most recent version of the MLF2 libraries and header files and installs them under `$PREFIX/mlf2/lib ` and `$PREFIX/mlf2/include`. The `$PREFIX` value defaults to `/prebuilt`.

All of the scripts require a certain set of environment variables to be set. Below is an example of a script that can be run on the host system to set everything up and run the container:

``` shell
#!/usr/bin/env bash
#
# Run the MLF2 Builder Docker container with this repository mounted
# under /$BITBUCKET_REPO_SLUG and the working directory set to /build
#
cat << EOF > .build_env
MLF2_PROJECT=smode
MLF2_DEPLOYMENT=calypso
GIT_USER=Michael Kenney
GIT_EMAIL=mfkenney@gmail.com
BITBUCKET_WORKSPACE=uwaplmlf2
BITBUCKET_REPO_OWNER=uwaplmlf2
BITBUCKET_REPO_SLUG=mlf2-smode
EOF

docker run -it --rm \
       --name "${BITBUCKET_REPO_SLUG}-build"
       --volume "${PWD}:/$BITBUCKET_REPO_SLUG" \
       --env-file .build_env \
       -e MISSION_BRANCH=$(git rev-parse --abbrev-ref HEAD) \
       --workdir "/build" \
       --entrypoint /bin/bash \
       mfkenney/mlf2builder:latest

```
