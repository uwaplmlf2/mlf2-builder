#!/usr/bin/env bash

IMGNAME=mlf2builder
ACCT=mfkenney

if type -p task; then
    task build
    [[ $1 = "release" ]] && task publish
    exit $?
fi

set -e
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)
VERSION="${vers#v}"
DATE="$(date +%FT%T)"
REV="$(git log -1 --pretty=format:%h%n)"

docker image build \
       --label=org.label-schema.version=${VERSION} \
       --label=org.label-schema.build-date=${DATE} \
       --label=org.label-schema.vcs-ref=${REV} \
       -t ${ACCT}/${IMGNAME}:${VERSION} -t  ${ACCT}/${IMGNAME}:latest .

if [[ $1 = "release" ]]; then
    docker image push -a ${ACCT}/${IMGNAME}
fi
