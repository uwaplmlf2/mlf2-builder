#!/usr/bin/env bash
#
# Integrate the latest revision of the ballasting code into MLF2 mission software.
#

src_repo="$1"
[[ -d "$src_repo" ]] || {
    echo "Source repository $src_repo not found" 1>&2
    exit 1
}

: ${MLF2_DEVFILE=DEVICES}

REPOS="https://bitbucket.org/$BITBUCKET_WORKSPACE"

[[ -z "$MLF2_PROJECT" || -z "$MLF2_DEPLOYMENT" ]] && {
    echo '$MLF2_PROJECT and $MLF2_DEPLOYMENT must be set' 1>&2
    exit 1
}

# Abort on any error
set -e

topdir="$(pwd)"

prjdir=mlf2-${MLF2_PROJECT}

git config --global core.autocrlf input
git config --global push.default simple
git clone --local --no-hardlinks "$src_repo" "$prjdir"
git clone --depth 1 ${REPOS}/mlf2-ballasting.git
git clone --depth 1 ${REPOS}/mlf2-lib.git
git clone --depth 1 ${REPOS}/tt8-lib.git

# Build the TT8 library
cd tt8-lib
scons destdir=./dist/ prefix="" install
export TT8LIBDIR="$PWD/dist/lib"
export TT8INCLUDE="$PWD/dist/include"

# Build the MLF2 library
cd ../mlf2-lib
scons destdir=./dist/ prefix="" install
export MLF2LIBDIR="$PWD/dist/lib"
export MLF2INCLUDE="$PWD/dist/include"

cd ../mlf2-ballasting
[[ -n "$OPS_BRANCH" ]] && git checkout "$OPS_BRANCH"
revid=$(git log -1 --pretty=format:%H%n)
echo "#define BALLAST_REVISION \"$revid\"" > ballastvers.h

# ... incorporate it into the mission software.
cd ../$prjdir
[[ -n "$MISSION_BRANCH" ]] && git checkout "$MISSION_BRANCH"
[[ -n "$GIT_USER" ]] && git config user.name "$GIT_USER"
[[ -n "$GIT_EMAIL" ]] && git config user.email "$GIT_EMAIL"

# Delete the "newballast" branch
git rev-parse --verify newballast > /dev/null && git branch -D newballast
git push origin :newballast || true

# Create a new version of the branch
git checkout -b newballast

cp -av ../mlf2-ballasting/* ballast/
sed -i -e 's/define SIMULATION/undef SIMULATION/' ballast/ballast.h
sed -e "/^#define [A-Z]\+/s/define/undef/" ballast/mtype.h > mtype.h.in
rm -f ballast/mtype.h
scons mtype=$MLF2_DEPLOYMENT devfile=$MLF2_DEVFILE

# If any changes have been made to the ballasting code, push the
# "newballast" branch and create a patchfile.
git add --all
patches=()
if git commit -m "Incorporated mlf2-ballasting:${revid:0:6}"; then
    rm -rf "${topdir}/patches"
    mkdir "${topdir}/patches"
    patches=($(git format-patch --diff-algorithm=minimal -o "${topdir}/patches" master))
    git push --set-upstream origin newballast || true
fi

cd "$topdir"
echo "Patches:"
for p in "${patches[@]}";do
    echo "    $p"
done
