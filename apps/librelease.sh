#!/usr/bin/env bash
#
# Build and publish a new release of an MLF2 library. This script
# should be run from the /build directory.
#

usage="Usage: ${0##*/} [-n] repository_dir"
while getopts "nh" opt; do
    case $opt in
        n)
            BITBUCKET_APP_PASSWORD=
            ;;
        h)
            echo $usage 1>&2
            exit 0
            ;;
    esac
done
shift $(($OPTIND - 1))

src_repo="$1"
[[ -d "$src_repo" ]] || {
    echo "Source repository $src_repo not found" 1>&2
    exit 1
}

name="${src_repo##*/}"
# Abort on any error
set -e

# Clone the repo
prjdir="$name"
rm -rf "$prjdir"
git clone --local --no-hardlinks "$src_repo" "$prjdir"

# Build the library and install under the build tree
cd "$prjdir"
./scripts/buildprep.sh
cmake --build build
cmake --build build -- install

rev=$(git log -1 --pretty=format:%h%n)
pkgname="${name}_${rev}.tar.gz"
tar -c -z -f "$pkgname" -C ./build/dist lib include

# Publish to Bitbucket or copy to dist directory under the source repo.
if [[ -n $BITBUCKET_APP_PASSWORD ]]; then
    ln "$pkgname" ${name}_latest.tar.gz
    echo "Publishing $pkgname to Bitbucket"
    bbupload.sh "$pkgname" ${name}_latest.tar.gz
else
    DIST="$src_repo/dist"
    rm -rf "$DIST"
    mkdir "$DIST"
    cp ${name}_${rev}.tar.gz "$DIST"
fi
