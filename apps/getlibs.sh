#!/usr/bin/env bash
#
# Download the latest revisions of the MLF2 libraries and header files and install
# them under $PREFIX
#

: ${PREFIX=/prebuilt}
: ${TT8LIB_URL="https://bitbucket.org/uwaplmlf2/tt8-lib"}
: ${MLF2LIB_URL="https://bitbucket.org/uwaplmlf2/mlf2-lib"}

set -e

topdir="$PREFIX/mlf2"
mkdir -p "$topdir"

[[ -e "$topdir/lib/libltt8.a" ]] || {
    # Download tt8 library
    curl -sS -L -X GET "${TT8LIB_URL}/downloads/tt8-lib_latest.tar.gz" |\
        tar -x -z -f - -C "$topdir"
}

[[ -e "$topdir/lib/libmlf2v2_1.a" ]] || {
    # Download mlf2 library
    curl -sS -L -X GET "${MLF2LIB_URL}/downloads/mlf2-lib_latest.tar.gz" |\
        tar -x -z -f - -C "$topdir"
}
