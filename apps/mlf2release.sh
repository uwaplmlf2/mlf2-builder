#!/usr/bin/env bash
#
# Build a specific revision of the MLF2 mission code from a mission
# software Git repository. The variables below are read from the
# environment
#
#     MLF2_PROJECT -- MLF2 project name
#     MLF2_DEPLOYMENT -- MLF2 deployment name (mission type)
#     MLF2_REPOS -- base of git repositories
#     MLF2_MISSION_REV -- git revision for mission software
#     MLF2_LIB_REV -- git revision for library software
#     MLF2_DEVFILE -- name of the DEVICES file
#
#

usage="Usage: ${0##*/} [-nh] repository_dir"
while getopts "n" opt; do
    case $opt in
        n)
            BITBUCKET_APP_PASSWORD=
            ;;
        h)
            echo $usage 1>&2
            exit 0
            ;;
    esac
done
shift $(($OPTIND - 1))

src_repo="$1"
[[ -d "$src_repo" ]] || {
    echo "Source repository $src_repo not found" 1>&2
    exit 1
}

# Default values (overridden by the environment)
: ${MLF2_DEVFILE=DEVICES}
: ${MLF2_REPOS=https://bitbucket.org/$BITBUCKET_WORKSPACE}

[[ -z "$MLF2_PROJECT" || -z "$MLF2_DEPLOYMENT" ]] && {
    echo '$MLF2_PROJECT and $MLF2_DEPLOYMENT must be set' 1>&2
    exit 1
}

# Abort on any error
set -e


prjdir=mlf2-${MLF2_PROJECT}

git config --global core.autocrlf input
git clone --local --no-hardlinks "$src_repo" "$prjdir"
git clone --depth 1 ${MLF2_REPOS}/mlf2-lib.git
git clone --depth 1 ${MLF2_REPOS}/tt8-lib.git

# Build the TT8 library
cd tt8-lib
TT8_LIB_REV=$(git log -n 1 --pretty=format:%H%n)
if [[ -e CMakeLists.txt ]]; then
    cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=cmake/mlf2gcc.cmake
    cmake --build build
    cmake --build build -- install
    export TT8LIBDIR="$PWD/build/dist/lib"
    export TT8INCLUDE="$PWD/build/dist/include"
else
    scons destdir=./dist/ prefix="" install
    export TT8LIBDIR="$PWD/dist/lib"
    export TT8INCLUDE="$PWD/dist/include"
fi

# Build the MLF2 library
cd ../mlf2-lib
[[ -n "$MLF2_LIB_REV" ]] && git checkout "$MLF2_LIB_REV"
revid=$(git log -n 1 --pretty=format:%H%n)
MLF2_LIB_REV="$revid"
if [[ -e CMakeLists.txt ]]; then
    cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=cmake/mlf2gcc.cmake
    cmake --build build
    cmake --build build -- install
    export MLF2LIBDIR="$PWD/build/dist/lib"
    export MLF2INCLUDE="$PWD/build/dist/include"
else
    scons destdir=./dist/ prefix="" install
    export MLF2LIBDIR="$PWD/dist/lib"
    export MLF2INCLUDE="$PWD/dist/include"
fi

# Build the mission software
cd ../$prjdir
[[ -n "$MISSION_REV" ]] && git checkout "$MISSION_REV"
revid=$(git log -n 1 --pretty=format:%H%n)
short_rev=$(git log -n 1 --pretty=format:%h%n)
MLF2_MISSION_REV="$revid"
if [[ -e CMakeLists.txt ]]; then
    cmake -S . -B build \
          -DCMAKE_TOOLCHAIN_FILE=cmake/mlf2gcc.cmake \
          -DMTYPE=$MLF2_DEPLOYMENT \
          -DDEVFILE=$MLF2_DEVFILE
    cmake --build build
    cmake --build build -- install
    distdir="build/dist"
else
    scons mtype=$MLF2_DEPLOYMENT devfile=$MLF2_DEVFILE
    distdir=".run"
fi

cat > "$distdir/build-params" <<EOF
MLF2_MISSION_REV=$MLF2_MISSION_REV
MLF2_LIB_REV=$MLF2_LIB_REV
TT8_LIB_REV=$TT8_LIB_REV
MLF2_PROJECT=$MLF2_PROJECT
MLF2_DEPLOYMENT=$MLF2_DEPLOYMENT
MLF2_REPOS=$MLF2_REPOS
MLF2_DEVFILE=$MLF2_DEVFILE
EOF

cd "$distdir"
pkgname="mlf2release-${MLF2_PROJECT}_${short_rev}.zip"
zip "$pkgname" build-params *.run

if [[ -n $BITBUCKET_APP_PASSWORD ]]; then
    ln "$pkgname" mlf2release-${MLF2_PROJECT}_latest.zip
    echo "Publishing $pkgname to Bitbucket"
    bbupload.sh "$pkgname" mlf2release-${MLF2_PROJECT}_latest.zip
else
    DIST="$src_repo/dist"
    rm -rf "$DIST"
    mkdir "$DIST"
    cp -v "$pkgname" "$DIST"
fi
